package lyra2_functions;

public class ReducedRoundBlaMka2Half {

	private static final int RHO = 1;

	public static void main(String[] args) {

		benchReducedSpongeLyraBlaMka();

	}

	private static void benchReducedSpongeLyraBlaMka() {
		long[] v = new long[] { 0x6a09e667f3bcc908L, 0xbb67ae8584caa73bL, 0x3c6ef372fe94f82bL, 0xa54ff53a5f1d36f1L,
				0x510e527fade682d1L, 0x9b05688c2b3e6c1fL, 0x1f83d9abfb41bd6bL, 0x5be0cd19137e2179L, 0x6a09e667f3bcc908L,
				0xbb67ae8584caa73bL, 0x3c6ef372fe94f82bL, 0xa54ff53a5f1d36f1L, 0x510e527fade682d1L, 0x9b05688c2b3e6c1fL,
				0x1f83d9abfb41bd6bL, 0x5be0cd19137e2179L };

		long times = 500_000_000;

		long start = System.nanoTime();

		for (int i = 0; i < times; i++) {
			reducedSpongeLyra(v);
		}

		long stop = System.nanoTime();
		System.out.println("elapsed time: " + (stop - start) + "ns");
		System.out.println("per execution: " + ((stop - start) / times) + "ns");
		System.out.println(Long.toHexString(v[0]));
	}

	private static void reducedSpongeLyra(long[] v) {
		// Sponge == haf-round BlaMka
		for (int i = 0; i < RHO; i++) {
			gblamka(v, 0, 4, 8, 12);
			gblamka(v, 1, 5, 9, 13);
			gblamka(v, 2, 6, 10, 14);
			gblamka(v, 3, 7, 11, 15);
			diagonalize(v);
		}
	}

	private static void gblamka(long[] v, int a, int b, int c, int d) {
		v[a] = fBlaMka(v[a], v[b]);
		v[d] = rotr64(v[d] ^ v[a], 32);
		v[c] = fBlaMka(v[c], v[d]);
		v[b] = rotr64(v[b] ^ v[c], 24);
		v[a] = fBlaMka(v[a], v[b]);
		v[d] = rotr64(v[d] ^ v[a], 16);
		v[c] = fBlaMka(v[c], v[d]);
		v[b] = rotr64(v[b] ^ v[c], 63);
	}

	private static void diagonalize(long[] v) {
		long t0, t1, t2;
		t0 = v[4];
		v[4] = v[5];
		v[5] = v[6];
		v[6] = v[7];
		v[7] = t0;
		t0 = v[8];
		t1 = v[9];
		v[8] = v[10];
		v[9] = v[11];
		v[10] = t0;
		v[11] = t1;
		t0 = v[12];
		t1 = v[13];
		t2 = v[14];
		v[12] = v[15];
		v[13] = t0;
		v[14] = t1;
		v[15] = t2;
	}

	private static long fBlaMka(long x, long y) {
		int lessX = (int) x;
		int lessY = (int) y;

		long lessZ = lessX & 0xffff_ffffL;

		lessZ = lessZ * ((long) lessY & 0xffff_ffffL);
		lessZ = lessZ << 1;

		long z = lessZ + x + y;

		return z;
	}

	private static long rotr64(long w, int c) {
		return (w >>> c) | (w << (64 - c));
	}

	private static void printBinary(long l) {
		for (int i = 0; i < Long.numberOfLeadingZeros((long) l); i++) {
			System.out.print('0');
		}
		System.out.println(Long.toBinaryString((long) l));
	}

	private static void printBinary(int j) {
		for (int i = 0; i < Integer.numberOfLeadingZeros((int) j); i++) {
			System.out.print('0');
		}
		System.out.println(Integer.toBinaryString((int) j));
	}

}
