package lyra2_functions;

import java.util.Random;

public class UnsignedModuloBench {

	public static void main(String[] args) {

		int times = 50_000_000;

		Random rand = new Random();
		long[] arr = new long[times];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextLong();
		}

		long start = System.nanoTime();
		for (int i = 0; i < times; i++) {

			arr[i] = Long.remainderUnsigned(arr[i], 255);

		}
		long stop = System.nanoTime();

		System.out.println("----- Long.remainderUnsigned() -----");
		System.out.println("time " + (stop - start) + "ns");
		System.out.println("time per execution " + ((stop - start) / times) + "ns");
		System.out.println(arr[0]);

		start = System.nanoTime();
		for (int i = 0; i < times; i++) {

			arr[i] = remainder(arr[i], 255);

		}
		stop = System.nanoTime();

		System.out.println("\n----- own remainder implementation -----");
		System.out.println("time " + (stop - start) + "ns");
		System.out.println("time per execution " + ((stop - start) / times) + "ns");
		System.out.println(arr[0]);

	}

	private static long remainder(long dividend, long divisor) {
		if (divisor < 0) { // i.e., divisor >= 2^63:
			if (Long.compare(dividend, divisor) < 0) {
				return dividend; // dividend < divisor
			} else {
				return dividend - divisor; // dividend >= divisor
			}
		}

		// Optimization - use signed modulus if dividend < 2^63
		if (dividend >= 0) {
			return dividend % divisor;
		}

		/*
		 * Otherwise, approximate the quotient, check, and correct if necessary.
		 * Our approximation is guaranteed to be either exact or one less than
		 * the correct value. This follows from the fact that floor(floor(x)/i)
		 * == floor(x/i) for any real x and integer i != 0. The proof is not
		 * quite trivial.
		 */
		long quotient = ((dividend >>> 1) / divisor) << 1;
		long rem = dividend - quotient * divisor;
		return rem - (Long.compare(rem, divisor) >= 0 ? divisor : 0);
	}

}
