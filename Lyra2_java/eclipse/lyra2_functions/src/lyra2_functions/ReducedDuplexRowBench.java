package lyra2_functions;

import java.util.Random;

public class ReducedDuplexRowBench {

	private static final int N_ROWS = 49152;
	private static final int N_COLS = 256;
	private static final int BLOCK_LEN_INT64 = 12;
	private static final int T = 1;

	public static void main(String[] args) {

		int times = 10;
		long extime = 0;

		for (int count = 0; count < times; count++) {

			long[][][] memMatrix = new long[N_ROWS][N_COLS][BLOCK_LEN_INT64];

			// initialize first 3 rows
			Random rand = new Random();
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < N_COLS; j++) {
					for (int k = 0; k < BLOCK_LEN_INT64; k++) {
						memMatrix[i][j][k] = rand.nextLong();
					}
				}
			}

			// initialize state with arbitrary values
			long[] state = new long[] { 0x6a09e667f3bcc908L, 0xbb67ae8584caa73bL, 0x3c6ef372fe94f82bL,
					0xa54ff53a5f1d36f1L, 0x510e527fade682d1L, 0x9b05688c2b3e6c1fL, 0x1f83d9abfb41bd6bL,
					0x5be0cd19137e2179L, 0x6a09e667f3bcc908L, 0xbb67ae8584caa73bL, 0x3c6ef372fe94f82bL,
					0xa54ff53a5f1d36f1L, 0x510e527fade682d1L, 0x9b05688c2b3e6c1fL, 0x1f83d9abfb41bd6bL,
					0x5be0cd19137e2179L };

			int gap = 1;
			int step = 1;
			int window = 2;
			int sqrt = 2;

			int row0 = 3;
			int prev0 = 2;
			int row1 = 1;
			int prev1 = 0;

			// ++++++++++++ setup phase +++++++++++++++++

			for (row0 = 3; row0 < N_ROWS; row0++) {
				reducedDuplexRowFilling(state, memMatrix, row1, prev0, prev1, row0);

				prev0 = row0;
				prev1 = row1;

				row1 = (row1 + step) & (window - 1);

				if (row1 == 0) {
					window *= 2;
					step = sqrt + gap;
					gap = -gap;
					if (gap == -1) {
						sqrt *= 2;
					}
				}
			}

			// ++++++++++++ wandering phase +++++++++++++++++

			long start = System.nanoTime();

			for (int i = 0; i < T * N_ROWS; i++) {
				row0 = (int) (remainder(state[0], N_ROWS));
				row0 = (int) (remainder(state[2], N_ROWS));

				reducedDuplexRowWandering(state, memMatrix, row0, row1, prev0, prev1);

				prev0 = row0;
				prev1 = row1;
			}

			long stop = System.nanoTime();
			extime += (stop - start);
			System.out.println(Long.toHexString(memMatrix[N_ROWS - 1][N_COLS - 1][0]));

		}

		System.out.println("time of one loop iteration" + (extime / times) + "ns");

	}

	private static long remainder(long dividend, long divisor) {
		if (divisor < 0) {
			if (Long.compare(flip(dividend), flip(divisor)) < 0) {
				return dividend;
			} else {
				return dividend - divisor;
			}
		}

		if (dividend >= 0) {
			return dividend % divisor;
		}

		long quotient = ((dividend >>> 1) / divisor) << 1;
		long rem = dividend - quotient * divisor;
		return rem - (Long.compare(flip(rem), flip(divisor)) >= 0 ? divisor : 0);
	}

	private static long flip(long a) {
		return a ^ Long.MIN_VALUE;
	}

	private static void reducedDuplexRowWandering(long[] state, long[][][] memMatrix, int rowInOut0, int rowInOut1,
			int rowIn0, int rowIn1) {
		long[][] ptrWordInOut0 = memMatrix[rowInOut0];
		long[][] ptrWordInOut1 = memMatrix[rowInOut1];
		long[] ptrWordIn0;
		long[] ptrWordIn1;
		int randomColumn0;
		int randomColumn1;

		int i, j;

		for (i = 0; i < N_COLS; i++) {
			randomColumn0 = (int) (remainder(state[4], N_COLS));
			ptrWordIn0 = memMatrix[rowIn0][randomColumn0];

			randomColumn1 = (int) (remainder(state[6], N_COLS));
			ptrWordIn1 = memMatrix[rowIn1][randomColumn1];

			for (j = 0; j < BLOCK_LEN_INT64; j++) {
				state[j] ^= (ptrWordInOut0[i][j] + ptrWordInOut1[i][j] + ptrWordIn0[j] + ptrWordIn1[j]);
			}

			// reducedSpongeLyra(state);

			for (j = 0; j < BLOCK_LEN_INT64; j++) {
				ptrWordInOut0[i][j] ^= state[j];
			}

			for (j = 0; j < BLOCK_LEN_INT64; j++) {
				ptrWordInOut1[i][j] ^= state[(j + 2) % BLOCK_LEN_INT64];
			}
		}
	}

	private static void reducedDuplexRowFilling(long[] state, long[][][] memMatrix, int rowInOut, int rowIn0,
			int rowIn1, int rowOut) {
		long[][] ptrWordIn0 = memMatrix[rowIn0];
		long[][] ptrWordIn1 = memMatrix[rowIn1];
		long[][] ptrWordInOut = memMatrix[rowInOut];
		long[][] ptrWordOut = memMatrix[rowOut];

		int i, j;

		for (i = 0; i < N_COLS; i++) {
			for (j = 0; j < BLOCK_LEN_INT64; j++) {
				state[j] ^= (ptrWordInOut[i][j] + ptrWordIn0[i][j] + ptrWordIn1[i][j]);
			}

			// reducedSpongeLyra(state);

			for (j = 0; j < BLOCK_LEN_INT64; j++) {
				ptrWordOut[N_COLS - i - 1][j] = ptrWordIn0[i][j] ^ state[j];
			}

			for (j = 0; j < BLOCK_LEN_INT64; j++) {
				ptrWordInOut[i][j] ^= state[(j + 2) % BLOCK_LEN_INT64];
			}
		}
	}

}
