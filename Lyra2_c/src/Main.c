/**
 * A simple main function for running the Lyra2 Password Hashing Scheme (PHS).
 * 
 * Author: The Lyra PHC team (http://www.lyra2.net/) -- 2015.
 * 
 * This software is hereby placed in the public domain.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>

#include <emmintrin.h>

#include "Lyra2.h"
#include "Sponge.h"

#ifndef BENCH
        #define BENCH 0
#endif

// ---------- begin macros blake2b tests ---------- 

#define _mm_roti_epi64(r, c) _mm_xor_si128(_mm_srli_epi64( (r), -(c) ),_mm_slli_epi64( (r), 64-(-c) ))

#define ROUND_BLAKE(r) \
  G1_BLAKE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G2_BLAKE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  DIAGONALIZE_BLAKE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G1_BLAKE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G2_BLAKE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  UNDIAGONALIZE_BLAKE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]);

#define G1_BLAKE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  row1l = _mm_add_epi64(row1l, row2l); \
  row1h = _mm_add_epi64(row1h, row2h); \
  \
  row4l = _mm_xor_si128(row4l, row1l); \
  row4h = _mm_xor_si128(row4h, row1h); \
  \
  row4l = _mm_roti_epi64(row4l, -32); \
  row4h = _mm_roti_epi64(row4h, -32); \
  \
  row3l = _mm_add_epi64(row3l, row4l); \
  row3h = _mm_add_epi64(row3h, row4h); \
  \
  row2l = _mm_xor_si128(row2l, row3l); \
  row2h = _mm_xor_si128(row2h, row3h); \
  \
  row2l = _mm_roti_epi64(row2l, -24); \
  row2h = _mm_roti_epi64(row2h, -24); \
 
#define G2_BLAKE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  row1l = _mm_add_epi64(row1l, row2l); \
  row1h = _mm_add_epi64(row1h, row2h); \
  \
  row4l = _mm_xor_si128(row4l, row1l); \
  row4h = _mm_xor_si128(row4h, row1h); \
  \
  row4l = _mm_roti_epi64(row4l, -16); \
  row4h = _mm_roti_epi64(row4h, -16); \
  \
  row3l = _mm_add_epi64(row3l, row4l); \
  row3h = _mm_add_epi64(row3h, row4h); \
  \
  row2l = _mm_xor_si128(row2l, row3l); \
  row2h = _mm_xor_si128(row2h, row3h); \
  \
  row2l = _mm_roti_epi64(row2l, -63); \
  row2h = _mm_roti_epi64(row2h, -63); \

#define DIAGONALIZE_BLAKE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  t0 = row4l;\
  t1 = row2l;\
  row4l = row3l;\
  row3l = row3h;\
  row3h = row4l;\
  row4l = _mm_unpackhi_epi64(row4h, _mm_unpacklo_epi64(t0, t0)); \
  row4h = _mm_unpackhi_epi64(t0, _mm_unpacklo_epi64(row4h, row4h)); \
  row2l = _mm_unpackhi_epi64(row2l, _mm_unpacklo_epi64(row2h, row2h)); \
  row2h = _mm_unpackhi_epi64(row2h, _mm_unpacklo_epi64(t1, t1))

#define UNDIAGONALIZE_BLAKE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  t0 = row3l;\
  row3l = row3h;\
  row3h = t0;\
  t0 = row2l;\
  t1 = row4l;\
  row2l = _mm_unpackhi_epi64(row2h, _mm_unpacklo_epi64(row2l, row2l)); \
  row2h = _mm_unpackhi_epi64(t0, _mm_unpacklo_epi64(row2h, row2h)); \
  row4l = _mm_unpackhi_epi64(row4l, _mm_unpacklo_epi64(row4h, row4h)); \
  row4h = _mm_unpackhi_epi64(row4h, _mm_unpacklo_epi64(t1, t1))

// ---------- end macros generic tests ---------- 


// ---------- begin macros blamka tests ----------

#define ROUND_LYRA_BLAMKA_SSE(r) \
  G1_BLAMKA_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G2_BLAMKA_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  DIAGONALIZE_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G1_BLAMKA_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G2_BLAMKA_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  UNDIAGONALIZE_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]);

#define ROUND_LYRA_BLAMKA_SSE_H(r) \
  G1_BLAMKA_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  G2_BLAMKA_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \
  DIAGONALIZE_SSE(v[0],v[2],v[4],v[6],v[1],v[3],v[5],v[7]); \

#define G1_BLAMKA_SSE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  row1l = fBlaMka_SSE(row1l, row2l); \
  row1h = fBlaMka_SSE(row1h, row2h); \
  \
  row4l = _mm_xor_si128(row4l, row1l); \
  row4h = _mm_xor_si128(row4h, row1h); \
  \
  row4l = _mm_roti_epi64_SSE(row4l, -32); \
  row4h = _mm_roti_epi64_SSE(row4h, -32); \
  \
  row3l = fBlaMka_SSE(row3l, row4l); \
  row3h = fBlaMka_SSE(row3h, row4h); \
  \
  row2l = _mm_xor_si128(row2l, row3l); \
  row2h = _mm_xor_si128(row2h, row3h); \
  \
  row2l = _mm_roti_epi64_SSE(row2l, -24); \
  row2h = _mm_roti_epi64_SSE(row2h, -24); \
 
#define G2_BLAMKA_SSE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  row1l = fBlaMka_SSE(row1l, row2l); \
  row1h = fBlaMka_SSE(row1h, row2h); \
  \
  row4l = _mm_xor_si128(row4l, row1l); \
  row4h = _mm_xor_si128(row4h, row1h); \
  \
  row4l = _mm_roti_epi64_SSE(row4l, -16); \
  row4h = _mm_roti_epi64_SSE(row4h, -16); \
  \
  row3l = fBlaMka_SSE(row3l, row4l); \
  row3h = fBlaMka_SSE(row3h, row4h); \
  \
  row2l = _mm_xor_si128(row2l, row3l); \
  row2h = _mm_xor_si128(row2h, row3h); \
  \
  row2l = _mm_roti_epi64_SSE(row2l, -63); \
  row2h = _mm_roti_epi64_SSE(row2h, -63); \

static inline __m128i fBlaMka_SSE(__m128i x, __m128i y){
    __m128i z = _mm_mul_epu32 (x, y);
    
    z = _mm_slli_epi64 (z, 1);
    
    z = _mm_add_epi64 (z, x);
    z = _mm_add_epi64 (z, y);
    
    return z;
}

#define _mm_roti_epi64_SSE(r, c) _mm_xor_si128(_mm_srli_epi64( (r), -(c) ),_mm_slli_epi64( (r), 64-(-c) ))

#define DIAGONALIZE_SSE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  t0 = row4l;\
  t1 = row2l;\
  row4l = row3l;\
  row3l = row3h;\
  row3h = row4l;\
  row4l = _mm_unpackhi_epi64(row4h, _mm_unpacklo_epi64(t0, t0)); \
  row4h = _mm_unpackhi_epi64(t0, _mm_unpacklo_epi64(row4h, row4h)); \
  row2l = _mm_unpackhi_epi64(row2l, _mm_unpacklo_epi64(row2h, row2h)); \
  row2h = _mm_unpackhi_epi64(row2h, _mm_unpacklo_epi64(t1, t1))

#define UNDIAGONALIZE_SSE(row1l,row2l,row3l,row4l,row1h,row2h,row3h,row4h) \
  t0 = row3l;\
  row3l = row3h;\
  row3h = t0;\
  t0 = row2l;\
  t1 = row4l;\
  row2l = _mm_unpackhi_epi64(row2h, _mm_unpacklo_epi64(row2l, row2l)); \
  row2h = _mm_unpackhi_epi64(t0, _mm_unpacklo_epi64(row2h, row2h)); \
  row4l = _mm_unpackhi_epi64(row4l, _mm_unpacklo_epi64(row4h, row4h)); \
  row4h = _mm_unpackhi_epi64(row4h, _mm_unpacklo_epi64(t1, t1))

// ---------- end macros sse tests ---------- 

/**
 * Generates the test vectors for Lyra2.
 *
 * @param t     Parameter to determine the processing time (T)
 * @param r     Memory cost parameter (defines the number of rows of the memory matrix, R)
 */
int testVectors(unsigned int t, unsigned int m_cost) {
    //=================== Basic variables, with default values =======================//
    int kLen = 64;
    unsigned char *pwd;
    int pwdLen = 11;
    unsigned char *salt;
    int saltLen = 16;

    srand(time(NULL));

    int i;
    int countSample;
    int indexSalt = 0;
    //==========================================================================/

    unsigned char *K = malloc(kLen);

    /* Generating vectors with the input size varying from 0 to 128 bytes,
     * and values varying from 0 to 127. The salt size is fixed in 16 bytes, 
     * and its value varies from 0 to 256.
     */
    for (countSample = 0; countSample <= 128; countSample++) {
        pwdLen = countSample;
        int count;
        pwd = malloc(sizeof (pwd) * pwdLen);
        for (count = 0; count < pwdLen; count++) {
                pwd[count] = count;
        }


        salt = malloc(sizeof (salt) * saltLen);
        for (count = 0; count < saltLen; count++) {
                salt[count] = saltLen * indexSalt + count;
        }
        indexSalt++;
        if (indexSalt == saltLen)
                indexSalt = 0;


        PHS(K, kLen, pwd, pwdLen, salt, saltLen, t, m_cost);

        printf("\ninlen: %d\n", pwdLen);
        printf("outlen: %d\n", kLen);
        printf("t_costs: %d\n", t);
        printf("m_costs: \tR: %d \tC: %d\n", m_cost, N_COLS);
        printf("parallelism: %u\n", nPARALLEL);
    
        char *spongeName ="";
        if (SPONGE==0){
            spongeName = "Blake2";
        }
        else if (SPONGE==1){
            spongeName = "BlaMka";
        }
        else{
            spongeName = "half-round BlaMka";
        }

        printf("sponge: %s\n", spongeName);
        printf("sponge blocks (bitrate): %u = %u bits\n", BLOCK_LEN_INT64, BLOCK_LEN_INT64*64);


        printf("In: ");
        for (i = 0; i < pwdLen; i++) {
                printf("%02x ", pwd[i]);
        }


        printf("\n");

        printf("Salt: ");
        for (i = 0; i < saltLen; i++) {
                printf("%02x ", salt[i]);
        }
        printf("\n");


        printf("Out: ");
        for (i = 0; i < kLen; i++) {
                printf("%02x ", K[i]);
        }
        printf("\n");
    }

    /* Generating vectors with the input size varying from 0 to 128 bytes,
     * and values varying from 128 to 255. The salt size is fixed in 16 bytes, 
     * and its value varies from 0 to 256.
     */
    for (countSample = 128; countSample <= 256; countSample++) {
	pwdLen = countSample - 127;
	int count;
	pwd = malloc(sizeof (pwd) * pwdLen);
	for (count = 0; count < pwdLen; count++) {
	    pwd[count] = count + 128;
	}

	salt = malloc(sizeof (salt) * saltLen);
	for (count = 0; count < saltLen; count++) {
	    salt[count] = saltLen * indexSalt + count;
	}
	indexSalt++;
	if (indexSalt == saltLen)
	    indexSalt = 0;

	PHS(K, kLen, pwd, pwdLen, salt, saltLen, t, m_cost);

	printf("\ninlen: %d\n", pwdLen);
        printf("outlen: %d\n", kLen);
        printf("t_costs: %d\n", t);
        printf("m_costs: \tR: %d \tC: %d\n", m_cost, N_COLS);
        printf("parallelism: %u\n", nPARALLEL);
    
        char *spongeName ="";
        if (SPONGE==0){
            spongeName = "Blake2";
        }
        else if (SPONGE==1){
            spongeName = "BlaMka";
        }
        else{
            spongeName = "half-round BlaMka";
        }

        printf("sponge: %s\n", spongeName);
        printf("sponge blocks (bitrate): %u = %u bits\n", BLOCK_LEN_INT64, BLOCK_LEN_INT64*64);

	printf("In: ");
	for (i = 0; i < pwdLen; i++) {
	    printf("%02x ", pwd[i]);
	}
	printf("\n");

	printf("Salt: ");
	for (i = 0; i < saltLen; i++) {
	    printf("%02x ", salt[i]);
	}
	printf("\n");

	printf("Out: ");
	for (i = 0; i < kLen; i++) {
	    printf("%02x ", K[i]);
	}
	printf("\n");
    }
    return 0;
}

void sse_bench(){

    __m128i *v = malloc(8 * sizeof (__m128i));
    if (v == NULL) {
        printf("error");
    }

    v[0] = _mm_load_si128((__m128i *) &blake2b_IV[0]);
    v[1] = _mm_load_si128((__m128i *) &blake2b_IV[2]);
    v[2] = _mm_load_si128((__m128i *) &blake2b_IV[4]);
    v[3] = _mm_load_si128((__m128i *) &blake2b_IV[6]);
    v[4] = _mm_load_si128((__m128i *) &blake2b_IV[0]);
    v[5] = _mm_load_si128((__m128i *) &blake2b_IV[2]);
    v[6] = _mm_load_si128((__m128i *) &blake2b_IV[4]);
    v[7] = _mm_load_si128((__m128i *) &blake2b_IV[6]);

    int n = 500000000;
    __m128i t0, t1;

    struct timeval start;
    struct timeval end;
    gettimeofday(&start, NULL);

    for (int i = 0; i < n; i++) {
    	ROUND_LYRA_BLAMKA_SSE(0);
        //ROUND_LYRA_BLAMKA_SSE_H(0);
    }

    gettimeofday(&end, NULL);
    unsigned long elapsed = (end.tv_sec-start.tv_sec)*1000000 + end.tv_usec-start.tv_usec;
    printf("execution time: %lu microseconds\n", elapsed);
    printf("per iteration: %luns\n", ((elapsed*1000)/n));

    int64_t *v64val = (int64_t*) &v[0];
    printf("results: %.16llx %.16llx\n", v64val[1], v64val[0]);
}

void generic_bench(){

    uint64_t test_v[16] =
    {
      0x6a09e667f3bcc908ULL, 0xbb67ae8584caa73bULL,
      0x3c6ef372fe94f82bULL, 0xa54ff53a5f1d36f1ULL,
      0x510e527fade682d1ULL, 0x9b05688c2b3e6c1fULL,
      0x1f83d9abfb41bd6bULL, 0x5be0cd19137e2179ULL,
      0x6a09e667f3bcc908ULL, 0xbb67ae8584caa73bULL,
      0x3c6ef372fe94f82bULL, 0xa54ff53a5f1d36f1ULL,
      0x510e527fade682d1ULL, 0x9b05688c2b3e6c1fULL,
      0x1f83d9abfb41bd6bULL, 0x5be0cd19137e2179ULL
    };

    uint64_t *v = test_v;
    
    int n = 500000000;

    struct timeval start;
    struct timeval end;
    gettimeofday(&start, NULL);

    for (int i = 0; i < n; i++) {
        ROUND_LYRA(0);
        //ROUND_LYRA_BLAMKA(0);
        //HALF_ROUND_LYRA_BLAMKA(0);
    }

    gettimeofday(&end, NULL);
    unsigned long elapsed = (end.tv_sec-start.tv_sec)*1000000 + end.tv_usec-start.tv_usec;
    printf("execution time: %lu microseconds\n", elapsed);
    printf("per iteration: %luns\n", ((elapsed*1000)/n));

    printf("%" PRIx64 "\n", v[0]);

}

int main(int argc, char *argv[]) {

	//sse_bench();
	//generic_bench();
	//return 0;

    //=================== Basic variables, with default values =======================//
    unsigned int kLen = 64;
    unsigned int t_cost = 0;
    unsigned int m_cost = 0;


    char *pwd = "Lyra2 PHS";
    unsigned int pwdLen = 9;
    char *salt = "saltsaltsaltsalt";
    unsigned int saltLen = 16;
    //==========================================================================/

    switch (argc) {
        case 2:
            if (strcmp(argv[1], "--help") == 0) {
                printf("Usage: \n");
                printf("       %s pwd salt kLen tCost nRows \n\n", argv[0]);
                printf("Inputs:\n");
                printf(" - pwd: the password\n");
                printf(" - salt: the salt\n");
                printf(" - kLen: output size\n");
                printf(" - tCost: the time cost parameter\n");
                printf(" - nRows: the number of rows parameter\n");
                printf("\n");
                printf("Or:\n");
                printf("       %s tCost nRows --testVectors     (to generate test vectors and test Lyra2 operation)\n\n", argv[0]);
                return 0;
            } else {
                printf("Invalid options.\nFor more information, try \"%s --help\".\n", argv[0]);
                return 0;
            }
            break;

        case 6:
            pwd = argv[1];
            pwdLen = strlen(pwd);
            salt = argv[2];
            saltLen = strlen(salt);
            kLen = atol(argv[3]);
            t_cost = atol(argv[4]);
            m_cost = atol(argv[5]);
            break;
        case 4:
            if (strcmp(argv[3], "--testVectors") == 0) {
                t_cost = atoi(argv[1]);
                m_cost = atoi(argv[2]);
                testVectors(t_cost, m_cost);
                return 0;
            } else {
                printf("Invalid options.\nFor more information, try \"%s --help\".\n", argv[0]);
                return 0;
            }
            break;

        default:
            printf("Invalid options.\nTry \"%s --help\".\n", argv[0]);
            return 0;
    }

    if (m_cost < 3) {
        printf("nRows must be >= 3\n");
        return 1;
    }

    if ((m_cost / 2) % nPARALLEL != 0) {
        printf("(nRows / 2) mod p must be = 0\n");
        return 1;
    }
    
    unsigned char *K = malloc(kLen);
   
    printf("Inputs: \n");
    printf("\tPassword: %s\n", pwd);
    printf("\tPassword Length: %u\n", pwdLen);
    printf("\tSalt: %s\n", salt);
    printf("\tSalt Length: %u\n", saltLen);
    printf("\tOutput Length: %u\n", kLen);
    printf("------------------------------------------------------------------------------------------------------------------------------------------\n");

    printf("Parameters: \n");
    printf("\tT: %u\n", t_cost);
    printf("\tR: %u\n", m_cost);
    printf("\tC: %u\n", N_COLS);
    printf("\tParallelism: %u\n", nPARALLEL);
    
    char *spongeName ="";
    if (SPONGE==0){
        spongeName = "Blake2";
    }
    else if (SPONGE==1){
        spongeName = "BlaMka";
    }
    else{
        spongeName = "half-round BlaMka";
    }
    
    printf("\tSponge: %s\n", spongeName);
    printf("\tSponge Blocks (bitrate): %u = %u bits\n", BLOCK_LEN_INT64, BLOCK_LEN_INT64*64);
    
    size_t sizeMemMatrix = (size_t) ((size_t)m_cost * (size_t)ROW_LEN_BYTES);

    if(sizeMemMatrix > (1610612736)){
    printf("\tMemory: %ld bytes (IMPORTANT: This implementation is known to have "
            "issues for such a large memory usage)\n", sizeMemMatrix);
    }else{
        printf("\tMemory: %ld bytes\n", sizeMemMatrix);
    }
        

    printf("------------------------------------------------------------------------------------------------------------------------------------------\n");
    
#if (BENCH == 1)
    struct timeval start;
    struct timeval end;
    gettimeofday(&start, NULL);
#endif
    int result;
    
    result = PHS(K, kLen, pwd, pwdLen, salt, saltLen, t_cost, m_cost);
    
#if (BENCH == 1)
    gettimeofday(&end, NULL);
    unsigned long elapsed = (end.tv_sec-start.tv_sec)*1000000 + end.tv_usec-start.tv_usec;
    printf("Execution Time: %lu microseconds\n", elapsed);
    printf("------------------------------------------------------------------------------------------------------------------------------------------\n");
#endif

    switch (result) {
        case 0:
            printf("Output: \n");

            printf("\n\tK: ");
            int i;
            for (i = 0; i < kLen; i++) {
                printf("%x|", K[i]);
            }
            break;
        case -1:
            printf("Error: unable to allocate memory (nRows too large?)\n");
            break;
        default:
            printf("Unexpected error\n");
            break;
    }

    printf("\n");
    printf("------------------------------------------------------------------------------------------------------------------------------------------\n");
    free(K);

    return 0;
}

